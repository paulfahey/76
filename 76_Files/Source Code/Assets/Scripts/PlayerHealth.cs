﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
	public GameObject Gun;
	public GameObject Reticle;
	public int CurrentHealth;
	public int StartHealth;

	public float flashLength;
	public float flashCounter;
	private SpriteRenderer rend;
	private Color storedColor;

	public Slider healthBar;
	// Use this for initialization
	Animator anim;
	Aim aim;

	public AudioClip deadsound;
	AudioSource audioSource;
	void Start(){
		aim = gameObject.GetComponent<Aim> ();
		anim= this.gameObject.GetComponent<Animator> ();
		CurrentHealth = 3;
		rend = this.gameObject.GetComponent<SpriteRenderer> ();
		storedColor = rend.color;
		CurrentHealth = StartHealth;
	}
	
	// Update is called once per frame
	void Update (){

		if (CurrentHealth <= 0) {
			anim.SetBool ("dead", true);

			Destroy (Gun);
			rend.color = storedColor;
			}
		if (flashCounter > 0) {
			rend.color = Color.red;
			flashCounter -= Time.deltaTime;

			if (flashCounter <= 0) {
				rend.color = storedColor;
			}
		}
	}

	void OnGUI()
	{
		healthBar.value = CurrentHealth;
	}

	public float shakeDuration;
	public void Hurt(int Damage){


		CurrentHealth -= Damage;
		rend.color = Color.red;
		flashCounter = flashLength;

	}
}
