﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		int i = 1;
		while (i < 5) {
			if (Input.GetAxis ("Submit" + "(" + i + ")") > 0.01) 
			{
				if (i == 1) 
				{
					GameManager.GM.player1Connect = true;	
				}
				if (i == 2) 
				{
					GameManager.GM.player2Connect = true;	
				}
				if (i == 3) 
				{
					GameManager.GM.player3Connect = true;	
				}if (i == 4) 
				{
					GameManager.GM.player4Connect = true;	
				}
			}
			i++;
		}
	}
}
