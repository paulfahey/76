﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour {
	public Transform Target;
    public Transform Player;
    GameObject parent;
    public GameObject reticle;
	public Transform ShootStartPos;
	public GameObject Aimer;
	public GameObject BulletPrefab;
	public GameObject Clone;
	public GameObject MainCam;
	Vector3 mousePos;
	Vector3 ObjPos;
	Vector3 AimPos;
	Transform Rot;
	float angle;

	public GameObject GameMan;
	GameManager GM;
	public bool faceRight;

	//
	public AudioClip shootsound;
	AudioSource audioSource;
	// Use this for initialization
	Animator anim;
	void Start(){
        parent = transform.parent.gameObject;
		audioSource = this.gameObject.GetComponent<AudioSource> ();
		GameMan = GameObject.FindGameObjectWithTag ("GameManager");
		GM = GameMan.GetComponent<GameManager> ();
		anim= GetComponentInParent<Animator> ();
		faceRight = true;
		Cursor.visible = false;
	}
	float distance = 10;
	// Update is called once per frame
	void Update () {
		if(GM.GameStarted){
		anim.SetBool("shooting", false);

            mousePos.x = Input.GetAxis("Mouse X"+ parent.GetComponent<PlayerRender>().thisPlayerIndex);
            mousePos.y = Input.GetAxis("Mouse Y"+ parent.GetComponent<PlayerRender>().thisPlayerIndex);
            //Debug.Log(Input.GetAxis("Fire1(1)"));
            //AimPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, distance));
            //ObjPos = Camera.main.WorldToScreenPoint(Target.position);
            //Aimer.transform.position = AimPos;

            //mousePos.x = mousePos.x - ObjPos.x;
            //mousePos.y = mousePos.y - ObjPos.y;

            //angle = Mathf.Atan2(mousePos.y, -mousePos.x) * Mathf.Rad2Deg;
            //Vector3 angleRot = new Vector3(0, 0, -angle);
            //transform.rotation = Quaternion.LookRotation(angleRot);

            angle = Mathf.Atan2(mousePos.y, -mousePos.x) * Mathf.Rad2Deg;
            //Vector2 shootDir = Vector2.right * Input.GetAxis("Mouse X") + Vector2.up * Input.GetAxis("Mouse Y");
            transform.rotation = Quaternion.Euler(0, 0, angle);

            Debug.DrawLine(ShootStartPos.position, mousePos, Color.red);
		//See if youre facing the right direction.
	
		CheckFace ();
		if (Input.GetAxis("Fire1"+"("+parent.GetComponent<PlayerRender>().thisPlayerIndex+")")>0.1f) {
			anim.SetBool("shooting", true);
			Shoot ();
		}

		if (ShakeTimer >= 0) {
	
			Vector2 ShakePos = Random.insideUnitCircle * ShakeAmount;
			MainCam.transform.position = new Vector3(MainCam.transform.position.x +ShakePos.x,MainCam.transform.position.y +ShakePos.y,MainCam.transform.position.z);
			ShakeTimer -= Time.deltaTime;
		}
	}
	}

	void CheckFace(){
		if (mousePos.x < Target.position.x+5)
		{
			faceRight = true;
		} 
		if (Target.position.x-5<mousePos.x) {
			faceRight = false;
		} 


		if (faceRight == true) {
			Target.transform.localScale = new Vector3 (1, 1, -1);
			Player.transform.localScale = new Vector3 (1, 1, 1);
			ShootStartPos.transform.localScale = new Vector3 (1, 1, 1);
		} else {
			Target.transform.localScale = new Vector3 (-1, -1, 1);
			Player.transform.localScale = new Vector3 (-1, 1, 1);
			ShootStartPos.transform.localScale = new Vector3 (-1, 1, 1);
		}
	}

	public float TimeBetweenShots=0.1f;
	private float timestamp;

	void Shoot(){
		if (Time.time >= timestamp) {
			audioSource.PlayOneShot (shootsound, 1f);
			Instantiate (BulletPrefab, ShootStartPos.position, transform.rotation);
			timestamp = Time.time + TimeBetweenShots;
            ShakeCam(0.5f);
		}
	}

	public float ShakeAmount;
	public float ShakeTimer;
	public void ShakeCam(float duration){
		
		ShakeTimer = duration;
	}
}
