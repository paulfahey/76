﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public int CurrentHealth;
	public int StartHealth;

	public float flashLength;
	public float flashCounter;
	private SpriteRenderer rend;
	private Color storedColor;

	public GameObject gameManager;
	WaveManager waveManager;

	// Use this for initialization
	void Start () {
		rend = this.gameObject.GetComponent<SpriteRenderer> ();
		storedColor = rend.color;
		CurrentHealth = StartHealth;
		gameManager = GameObject.Find ("GameManager");
		waveManager = gameManager.GetComponent<WaveManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if (CurrentHealth <= 0) {
			GameManager.GM.Score++;
			waveManager.EnemyCount--;
			Destroy (gameObject);
		}
		if (flashCounter > 0) {
			rend.color = Color.red;
			flashCounter -= Time.deltaTime;

			if (flashCounter <= 0) {
				rend.color = storedColor;
			}
		}
	}

	public void Hurt(int Damage){
		
		CurrentHealth -= Damage;
		rend.color = Color.red;
		flashCounter = flashLength;

	}
}
