﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
	public float speed;
	Animator anim;
	public PlayerHealth PH;
    PlayerRender playerRend;
	Scene LoadedLevel;
	GameManager GM;
	GameObject GameMan;
	Pickup pickUpScript;

	void Start ()
	{
        playerRend = GetComponent<PlayerRender>();
		anim = this.gameObject.GetComponent<Animator> ();
		PH = this.gameObject.GetComponent<PlayerHealth> ();
		GameMan = GameObject.FindGameObjectWithTag ("GameManager");
		GM = GameMan.GetComponent<GameManager> ();
	}
	// Update is called once per frame
	void Update ()
	{
		if (GM.GameStarted) {
			if (PH.CurrentHealth > 0) {
				anim.SetBool ("moving", false);
				if (Input.GetAxis("Horizontal"+playerRend.thisPlayerIndex.ToString())>0.01f) {
					anim.SetBool ("moving", true);
					transform.Translate (Vector2.right * speed * Time.deltaTime);
                    GetComponent<SpriteRenderer>().flipX = false;
				}
				if (Input.GetAxis("Horizontal" + playerRend.thisPlayerIndex.ToString()) < -0.01f) {
					anim.SetBool ("moving", true);
					transform.Translate (-Vector2.right * speed * Time.deltaTime);
                    GetComponent<SpriteRenderer>().flipX = true;
                }
				if (Input.GetAxis("Vertical" + playerRend.thisPlayerIndex.ToString()) > 0.01f) {
					anim.SetBool ("moving", true);
					transform.Translate (Vector2.up * speed * Time.deltaTime);
				}
				if (Input.GetAxis("Vertical" + playerRend.thisPlayerIndex.ToString()) < -0.01f) {
					anim.SetBool ("moving", true);
					transform.Translate (-Vector2.up * speed * Time.deltaTime);  
				} 
			}
		}
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.tag == "Pickup") 
		{
			pickUpScript = col.gameObject.GetComponent<Pickup>();
			PickupActivate(pickUpScript.thisType);
			Destroy(col.gameObject);
		}
	}

	public void PickupActivate(Pickup.Type itemType)
	{
		if (itemType == Pickup.Type.Health) {
			PH.CurrentHealth += 5;
		} else if (itemType == Pickup.Type.DamageUp) {
			//Increase weapon damage
		} else if (itemType == Pickup.Type.Invincibility) {
			//Turn canTakeDamage off
		} else if (itemType == Pickup.Type.Nuke) {
			//Iterate through enemies and destroy
		} else if (itemType == Pickup.Type.Weapon) {
			//Change weapon type
		}
	}
}

