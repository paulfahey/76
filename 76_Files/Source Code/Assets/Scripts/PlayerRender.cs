﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRender : MonoBehaviour {
	SpriteRenderer ThisRen;
	SpriteRenderer GunRen;
	public GameObject Gun;
	public GameObject Shadow;
	public PlayerHealth PH;
	SpriteRenderer Sha;
	public int thisPlayerIndex; 

	// Use this for initialization
	void Start () {
		PH = this.gameObject.GetComponentInParent<PlayerHealth>();
		Sha = Shadow.GetComponent<SpriteRenderer>();
		ThisRen = this.gameObject.GetComponent<SpriteRenderer>();
		//ThisRen.sprite = Resources.Load<Sprite>("Sprites/Players/"+thisPlayerIndex.ToString());
		GunRen= Gun.GetComponent<SpriteRenderer>();
		GunRen.sortingOrder = (ThisRen.sortingOrder + 10);
		ThisRen.sortingOrder = -(int)(transform.position.y * 100);
	}
	
	// Update is called once per frame
	void Update () {
		if (PH.CurrentHealth>0) {
			ThisRen.sortingOrder = -(int)(transform.position.y * 100);
			GunRen.sortingOrder = (ThisRen.sortingOrder * 10);
			Sha.sortingOrder = ThisRen.sortingOrder - 1;
		}
	}
}
