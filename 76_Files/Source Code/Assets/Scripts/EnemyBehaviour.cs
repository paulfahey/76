﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
	public GameObject Player;
	Vector3 ThisPos;
	Vector3 PlayerPos;
	SpriteRenderer ThisRen;
	public GameObject Shadow;
	SpriteRenderer Sha;
	SpriteRenderer PlayerRen;
	PlayerHealth PlayerH;
	Rigidbody2D rb;
	public AudioClip deadsound;
	AudioSource audioSource;
	void Start(){
		audioSource = this.gameObject.GetComponent<AudioSource> ();
		Player= GameObject.FindGameObjectWithTag ("Player");
		ThisRen = this.gameObject.GetComponent<SpriteRenderer> ();
		PlayerH = Player.gameObject.GetComponent<PlayerHealth> ();
		Sha = Shadow.GetComponent<SpriteRenderer> ();
		rb = this.gameObject.GetComponent<Rigidbody2D> ();
		ThisRen.sortingOrder = -(int)(transform.position.y * 100);
	}
	public float speed;
	float oldy;


	// Update is called once per frame
	void Update () {
		
		ThisRen.sortingOrder = -(int)(transform.position.y * 100);
		Sha.sortingOrder = ThisRen.sortingOrder - 1;
		//GET THE PLAYER POS AND THIS POSITION
		if (PlayerH.CurrentHealth > 0) {
			PlayerPos = Player.transform.position;

			ThisPos = this.gameObject.transform.position;

            if (PlayerPos.x > ThisPos.x + 0.5f)
            {
                this.gameObject.transform.localScale = new Vector3(-1, 1, 1);
            }
            else if (PlayerPos.x < ThisPos.x - 0.5f)
            {
                this.gameObject.transform.localScale = new Vector3(1, 1, 1);
            }

            if (Vector2.Distance (ThisPos, PlayerPos) >= 0) {

				transform.position = Vector2.MoveTowards (ThisPos, PlayerPos, speed * Time.deltaTime);
			}

		}
	}
	public float TimeBetweenDamage=0.1f;
	private float timestamp;
	void OnCollisionStay2D(Collision2D coll){
		if (coll.gameObject.tag == "Player") {
			if (Time.time >= timestamp) {
				PlayerHealth PH = coll.gameObject.GetComponent<PlayerHealth> ();
				PH.Hurt (1);
				audioSource.PlayOneShot (deadsound, 1);
				timestamp = Time.time + TimeBetweenDamage;
			}
		}
	}
}
