﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {

	public static GameManager GM;
	public GameObject ScoreTextObj;
	public GameObject StartTxt;
	public GameObject Player;
	PlayerHealth PH;
	bool GamePaused = false;
	public 	bool GameStarted=false;
	public int Score=0;
	public Text Scoretxt;
	public Text EndTxt;

	public bool player1Connect;
	public bool player2Connect;
	public bool player3Connect;
	public bool player4Connect;

	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	public GameObject player4;

	public Text player1Text;
	public Text player2Text;
	public Text player3Text;
	public Text player4Text;

	// Use this for initialization
	void Start () {
		if(GM == null)
        {
            GM = this;
        }
        else if(GM != this)
        {
            Destroy(GM.gameObject);
        }

        //DontDestroyOnLoad(this.gameObject);
		ScoreTextObj.SetActive (false);
		StartTxt.SetActive (true);
		//GameStarted = true;

		player1 = GameObject.Find ("Player1");
		player2 = GameObject.Find ("Player2");
		player3 = GameObject.Find ("Player3");
		player4 = GameObject.Find ("Player4");

		if (!player1Connect) {
			player1.SetActive(false);
		}
		if (!player2Connect) {
			player2.SetActive(false);
		}
		if (!player3Connect) {
			player3.SetActive(false);
		}
		if (!player4Connect) {
			player4.SetActive(false);
		}
		//PH = Player.GetComponent<PlayerHealth> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetAxis("Submit(1)")>0.1f){
            if (!GameStarted)
            {
                GameStarted = true;
                ScoreTextObj.SetActive(true);
                StartTxt.SetActive(false);
            }
         
        }
        if (Input.GetButton("Start"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        //		if (PH.CurrentHealth <= 0) {
        //			EndTxt.text = ("Gameover, your score was:" + Score);
        //			StartTxt.SetActive (true);
        //		}
    }

    private void OnGUI()
    {
        Scoretxt.text = ("Score:" + Score);
    }

}
