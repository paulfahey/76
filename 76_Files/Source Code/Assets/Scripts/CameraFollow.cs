﻿using UnityEngine;
using System.Collections;

public class CameraFollow: MonoBehaviour {

    //public float dampTime = 0.15f;
    //public float height =0.4f;
    //private Vector3 velocity = Vector3.zero;
    //public Transform[] targets;

    //float leftside=100;
    //float rightside= Screen.width-100;
    //float topside= Screen.height-200;
    //float bottomside=200;
    //public float mouseSpeed;

    public Transform[] targets;

    float boundingBoxPadding = 2f;

    float minimumOrthoSize = 8f;

    float zoomSpeed = 20f;

    Camera camera;


    // Update is called once per frame

    void Awake()
    {
        camera = GetComponent<Camera>();
        camera.orthographic = true;
    }

    private void LateUpdate()
    {
        Rect boundingBox = CalculateTargetsBoundingBox();
        transform.position = CalculateCameraPosition(boundingBox);
        camera.orthographicSize = CalculateOrthographicSize(boundingBox);
    }

    Rect CalculateTargetsBoundingBox()
    {
        float minX = Mathf.Infinity;
        float maxX = Mathf.NegativeInfinity;
        float minY = Mathf.Infinity;
        float maxY = Mathf.NegativeInfinity;

        foreach (Transform target in targets)
        {
            Vector3 position = target.position;

            minX = Mathf.Min(minX, position.x);
            minY = Mathf.Min(minY, position.y);
            maxX = Mathf.Max(maxX, position.x);
            maxY = Mathf.Max(maxY, position.y);
        }

        return Rect.MinMaxRect(minX - boundingBoxPadding, maxY + boundingBoxPadding, maxX + boundingBoxPadding, minY - boundingBoxPadding);
    }

    
    Vector3 CalculateCameraPosition(Rect boundingBox)
    {
        Vector2 boundingBoxCenter = boundingBox.center;

        return new Vector3(boundingBoxCenter.x, boundingBoxCenter.y, camera.transform.position.z);
    }

    
    float CalculateOrthographicSize(Rect boundingBox)
    {
        float orthographicSize = camera.orthographicSize;
        Vector3 topRight = new Vector3(boundingBox.x + boundingBox.width, boundingBox.y, 0f);
        Vector3 topRightAsViewport = camera.WorldToViewportPoint(topRight);

        if (topRightAsViewport.x >= topRightAsViewport.y)
            orthographicSize = Mathf.Abs(boundingBox.width) / camera.aspect / 2f;
        else
            orthographicSize = Mathf.Abs(boundingBox.height) / 2f;

        return Mathf.Clamp(Mathf.Lerp(camera.orthographicSize, orthographicSize, Time.deltaTime * zoomSpeed), minimumOrthoSize, Mathf.Infinity);
    }

    void Update () 
	{
		//	Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
		//	Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, height, point.z)); //(new Vector3(0.5, 0.5, point.z));
		//	Vector3 destination = transform.position + delta;
		//	transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

		//if (Input.mousePosition.x > rightside) {
			
		//	transform.position += new Vector3 (10, 0, 0) * Time.deltaTime * mouseSpeed *dampTime;
		//}
		//if (Input.mousePosition.x < leftside) {
		//	transform.position -= new Vector3 (10, 0, 0) * Time.deltaTime *  mouseSpeed *dampTime;
		//}
	
		//if (Input.mousePosition.y < bottomside) {
		//	transform.position -= new Vector3 (0, 10, 0) * Time.deltaTime *  mouseSpeed *dampTime;
		//}
		//if (Input.mousePosition.y > topside) {
		//	transform.position += new Vector3 (0, 10, 0) * Time.deltaTime * mouseSpeed * dampTime;
		//} else {
		//	transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

		//}
	
	}

}