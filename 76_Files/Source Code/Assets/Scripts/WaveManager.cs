﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour
{
	public GameManager GM;
	public int CurrentWave;

	public GameObject Spawner1;
	public GameObject Spawner2;
	public GameObject Spawner3;
	public GameObject Spawner4;
	public GameObject Spawner5;
	public GameObject Spawner6;
	public GameObject Spawner7;
	public GameObject Spawner8;

	Vector3 S1Pos;
	Vector3 S2Pos;
	Vector3 S3Pos;
	Vector3 S4Pos;
	Vector3 S5Pos;
	Vector3 S6Pos;
	Vector3 S7Pos;
	Vector3 S8Pos;

	public GameObject Zombie;
    public GameObject ZombieFast;
    public GameObject boss;
	public int EnemyCount;
	public int maxEnemies = 20;
	public int SpawnEnemies;
	public bool canSpawn = true;
	public Text waveText;

	// Use this for initialization
	void Start ()
	{
		GM = this.gameObject.GetComponent<GameManager> ();
		S1Pos = Spawner1.transform.position;
		S2Pos = Spawner2.transform.position;
		S3Pos = Spawner3.transform.position;
		S4Pos = Spawner4.transform.position;
		S5Pos = Spawner5.transform.position;
		S6Pos = Spawner6.transform.position;
		S7Pos = Spawner7.transform.position;
		S8Pos = Spawner8.transform.position;


	}

	public int SpawnPOS;
	Vector3 SpwPos;
	public float TimeBetweenSpawn = 0.5f;
	private float timestamp;

	int ZombieFastSpawn = 0;


	void OnGUI()
	{
		waveText.text = "Current Wave:" + CurrentWave.ToString ();
	}

	void Update()
	{
		while (GM.GameStarted && canSpawn) {
			StartCoroutine(SpawnWaves());
		}
		if (EnemyCount <= 0 && !canSpawn) {
			float timer = 0;

				EndWave ();

		}
	}

	void SpawnZombie ()
	{
		if (CurrentWave < 4) {
			if (ZombieFastSpawn == 0) {
                Instantiate(Zombie, SpwPos, Quaternion.Euler(0, 0, 0));
            } else {
                Instantiate(ZombieFast, SpwPos, Quaternion.Euler(0, 0, 0));
            }
			//EnemyCount++;
			SpawnPOS = Random.Range (1, 4);
			timestamp = Time.time + TimeBetweenSpawn;
		} else if (CurrentWave >= 4) {
			int roll = Random.Range (0, 3);
			{
				if (roll == 0) {
                    Instantiate(boss, SpwPos, Quaternion.Euler(0, 0, 0));
				} else {
					if (ZombieFastSpawn == 0) {
						Instantiate (Zombie, SpwPos, Quaternion.Euler (0, 0, 0));
					} else {
						Instantiate (ZombieFast, SpwPos, Quaternion.Euler (0, 0, 0));
					}

					//EnemyCount++;
					SpawnPOS = Random.Range (1, 4);
					timestamp = Time.time + TimeBetweenSpawn;
				}
			}

		}
	}

	IEnumerator SpawnWaves()
	{
		SpawnPOS = Random.Range (1, 8);
		ZombieFastSpawn = Random.Range (-1, 1);
			{
				switch (SpawnPOS) {
					case 1:
						SpwPos = S1Pos;
						SpawnZombie ();

						break;
					case 2:
						SpwPos = S2Pos;
						SpawnZombie ();

						break;
					case 3:
						SpwPos = S3Pos;
						SpawnZombie ();

						break;
					case 4:
						SpwPos = S4Pos;
						SpawnZombie ();

						break;
					case 5:
						SpwPos = S5Pos;
						SpawnZombie ();

						break;
					case 6:
						SpwPos = S6Pos;
						SpawnZombie ();

						break;
					case 7:
						SpwPos = S7Pos;
						SpawnZombie ();

						break;
					case 8:
						SpwPos = S8Pos;
						SpawnZombie ();

						break;
				}
			EnemyCount++;
			if (EnemyCount >= maxEnemies) 
			{
				canSpawn = false;
			}
			yield return new WaitForSeconds(2);
			}
	}

	void EndWave()
	{
		CurrentWave++;
		maxEnemies += 5;
		EnemyCount = 0;
		canSpawn = true;
	}
}

