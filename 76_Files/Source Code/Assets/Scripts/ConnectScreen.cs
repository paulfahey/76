﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetAxis("Submit(1)") > 0.01)
        {
            GameManager.GM.player1Connect = true;
        }
        if (Input.GetAxis("Submit(2)") > 0.01)
        {
            GameManager.GM.player2Connect = true;
        }
        if (Input.GetAxis("Submit(3)") > 0.01)
        {
            GameManager.GM.player3Connect = true;
        }
        if (Input.GetAxis("Submit(4)") > 0.01)
        {
            GameManager.GM.player4Connect = true;
        }
    }
}
