﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {


	private Sprite defaultSprite;
	public Sprite muzzleFlash;
	public int FramesToFlash = 3;
	public float destroyTime=3;
	SpriteRenderer ThisRen;
	//BEHAVIOUR VARS
	public float speed;
	Rigidbody2D rbBullet;
	public float TimeAlive;
	public int BulletDuration;

	// Use this for initialization
	void Start () {
		//Sprites
		ThisRen = this.gameObject.GetComponent<SpriteRenderer> ();
		defaultSprite = ThisRen.sprite;
		StartCoroutine (FlashMuzzleFlash ());
	

		rbBullet = this.gameObject.GetComponent<Rigidbody2D> ();
		rbBullet.AddForce (this.transform.right * -speed);
	
	}
	
	// Update is called once per frame
	public float duration = 10;


	void Update () {
		
		TimeAlive += Time.deltaTime;


		if (TimeAlive >= BulletDuration) {
			
			DestroyObject (this.gameObject);

		}

}

	IEnumerator FlashMuzzleFlash(){
		ThisRen.sprite = muzzleFlash;
		for (int i = 0; i < FramesToFlash; i++) {
			yield return 0;
		}
		ThisRen.sprite = defaultSprite;
	}

	public int damage;
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Enemy") {
			EnemyHealth EH = coll.gameObject.GetComponent<EnemyHealth>();
			EH.Hurt (damage);
            Destroy(gameObject);
        }
        if(coll.gameObject.tag == "Player")
        {
            PlayerHealth PH = coll.gameObject.GetComponent<PlayerHealth>();
            PH.Hurt(damage/2);
            Destroy(gameObject);
        }
	}
}
